#pragma once
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include "..\cryptopp565\osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
using namespace std;

#include <string>
using std::string;

#include <cstdlib>
using std::exit;

#include "..\cryptopp565\cryptlib.h"
using CryptoPP::Exception;

#include "..\cryptopp565\hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "..\cryptopp565\filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include "..\cryptopp565\des.h"
using CryptoPP::DES_EDE2;

#include "..\cryptopp565\modes.h"
using CryptoPP::CBC_Mode;

#include "..\cryptopp565\secblock.h"
using CryptoPP::SecByteBlock;
#include <iostream>
#include <string>
#include "..\cryptopp565\modes.h"
#include "..\cryptopp565\aes.h""
#include "..\cryptopp565\filters.h"

std::string key = "1F0b755A0853BF7C6934AB5314572CBF";
std::string decryptedtext;



//converts hex characters in a string to Ascii
std::string hexToAscii(std::string hex) {
	int len = hex.length();
	std::string newString;
	for (int i = 0; i< len; i += 2)
	{
		string byte = hex.substr(i, 2);
		char chr = (char)(int)strtol(byte.c_str(), NULL, 16);
		newString.push_back(chr);
	}

	return newString;
}


//makes vector a string
std::string stringify(std::vector<char> vect) {
	std::string temp = "";
	int size = vect.size();
	for (int i = 0; i < size; i++) {
		temp += vect[i];
	}

	return temp;
}



//encryption, but not used so don't bother
std::string encryption(std::string plaintext, std::string key, std::string iv) {

	std::string ciphertext;
	key = hexToAscii(key);
	CryptoPP::AES::Encryption aesEncryption((byte *)key.c_str(), CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, (byte *)iv.c_str());

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(ciphertext));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plaintext.c_str()), plaintext.length() + 1);
	stfEncryptor.MessageEnd();

	return ciphertext;
}

//takes a ciphertext string and breaks it up into blocksof 16 bytes
std::vector<string> blockify(std::string str, std::string iv) {
	int n = str.size();
	std::string temp = "";
	std::vector<std::string> blocks;
	blocks.push_back(iv);

	for (int i = 0; i <= n; i++) {
		if (i % 16 != 0 || i == 0) {
			temp += str[i];
		}
		else {
			blocks.push_back(temp);
			temp.clear();
			temp += str[i];
		}

	}

	return blocks;
}

//overloaded without IV
std::vector<string> blockify(std::string str) {
	int n = str.size();
	std::string temp = "";
	std::vector<std::string> blocks;

	for (int i = 0; i <= n; i++) {
		if (i % 16 != 0 || i == 0) {
			temp += str[i];
		}
		else {
			blocks.push_back(temp);
			temp.clear();
			temp += str[i];
		}

	}

	return blocks;
}

//AES functions to decrypt. if there is a padding error, returns "nay", otherwise returns the decryptedtext.
std::string decrypt(std::string ciphertext, std::string iv, std::string key) {
	std::string decryptedtext;
	key = hexToAscii(key);


	try
	{
		CryptoPP::AES::Decryption aesDecryption((byte *)key.c_str(), CryptoPP::AES::DEFAULT_KEYLENGTH);
		CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, (byte *)iv.c_str());
		CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedtext));

		stfDecryptor.Put(reinterpret_cast<const unsigned char*>(ciphertext.c_str()), ciphertext.size());
		stfDecryptor.MessageEnd();
	
		return decryptedtext;
	}
	catch (const CryptoPP::Exception& e)
	{
		//cout << e.what() << endl; system("pause");
		return "nay";
	}


	return decryptedtext;
}


//should return true if the decryption was successful or false if there was a padding error.
bool oracle(std::string ciphertext, std::string iv) {
	string temp = decrypt(ciphertext, iv, key);
	if (temp == "nay")
		return false;
	else
		return true;
}



template <typename T>
void displayVector(vector<T> vect) {
	int size = vect.size();
	for (int i = 0; i < size; i++) {
		cout << vect[i] << " ";
	}
}