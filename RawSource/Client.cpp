//#include "stdafx.h"

// g++ -g3 -ggdb -O0 -DDEBUG -I/usr/include/cryptopp Driver.cpp -o Driver.exe -lcryptopp -lpthread
// g++ -g -O2 -DNDEBUG -I/usr/include/cryptopp Driver.cpp -o Driver.exe -lcryptopp -lpthread
#include <cstdio>
#include <iostream>
#include <fstream>
#include <vector>
#include "OracleServer.h"
#include "..\cryptopp565\osrng.h"
using CryptoPP::AutoSeededRandomPool;

#include <iostream>
using std::cout;
using std::cerr;
using std::endl;
using namespace std;

#include <string>
using std::string;

#include <cstdlib>
using std::exit;

#include "..\cryptopp565\cryptlib.h"
using CryptoPP::Exception;

#include "..\cryptopp565\hex.h"
using CryptoPP::HexEncoder;
using CryptoPP::HexDecoder;

#include "..\cryptopp565\filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#include "..\cryptopp565\des.h"
using CryptoPP::DES_EDE2;

#include "..\cryptopp565\modes.h"
using CryptoPP::CBC_Mode;

#include "..\cryptopp565\secblock.h"
using CryptoPP::SecByteBlock;
#include <iostream>
#include <string>
#include <fstream>
#include "..\cryptopp565\modes.h"
#include "..\cryptopp565\aes.h""
#include "..\cryptopp565\filters.h"


std::vector<string> plaintext;
std::string ciphertext;
int numberOfPaddingBlocks;
int findPadding(std::string&, std::string, std::string);
void attackMessage(int, std::string, std::string);
ofstream fout;

int main(int argc, char* argv[]) {
	std::string iv = "756ed598101c9733635517ff2477ECB5";

	fout.open("output.txt");
	//ciphertext hardcoded into the program
	ciphertext = "7832B541207F89A648BAED328A40B3439A24096150A03FACC57122DE622C7E905A959A7DB320FA5FD33305071E22227ED3A4EDBE697DF873F9BE59E97EB4B0CD9F4006B7F0BF556E6DFB84A171B7068DC4A9AC99D08E05BB2457FC98756DCE0C8A1A009C006DCC89E8D65F6F000B5E29FDFA50CB7F5A49E206313A822AE7382A405DE835D4E9B6DBE6D6AB0180889550516075C7706E29E9A2580E89C3262D133D2C7BEBFAD0F2515D63DFCBC7D6ADC1492F438F5E59C8D1DED3EC25FB554D4DB50E4EA59D7D8060B24B1D10CD29F612D776083BBA157AC9DB06F9A19ABD960BC66231DFB88DD9DEA31F689FF04B36AFF6A3CC57A1B8561C8C5A6C3FDA52235C106A102B18221C48BC03371D7B5DD0135FD90FFEBAD574132CD86BB3775441D09C621F08768537AC1F5D01CEA245FB7BB4DF7B8B9AE1748CEA30FB503005FC39884C6DFD952A3B0D0413DC0867BF956C27ADBCA6368F3AD13181D6DE814C124183456744343766BA8F7EC6282742C455AF4891346D816024C0C6AF8B24C3285376E4CB97663F82C79E30D2C50529517E55B04637F9BDB97344B931CE8F51EB48CFF568D3A470E4B50548D455DA80D668433272EF3F5DF9E75320F16292E3F673F26F5E43CD16AB31A17861B3B3551609D4BAC83903DBD3043B40912026100E4235804B10861274AC3E559F2CB15F81387929B90C1AD9DF4243D83B034708963EC899BBD4FFFF3812D29033732591CD24206C9D571F83917B6EC79A12E353D7248CE0F92216DC8DC977F712E51BF8B8CDA5305584C80138FBF8C4D59CE0883FAB3BEB082A0DDF2F0C6D319EC347688C4E21231F9FBC4CF537CDC748597D81FB0073146E85DF3F165D03C95BCBA5B145AD505EEC43146D25F712F16E409ED6DDC468E9F2AF4E383EE14B05D2412CFF93E1DBA25A6B07A3BD7A465467E7357C680501B374C8356F796B8170D30858D52DBBF8FDE2E35FCEF283762D2E0CE7739E8DC899B3954AE90DE222FBA007170FDF6A10B60A5FE570DF96303E11A3C22BABE30DD11B144F79A9E40D1A6E1A86845F94078B1E43055EC48394AD0FFD579657E30D7BA3FCAD71A98BABB2B4C9632A8392AF8EF9DBB940D0ABA706F1E3A11FCC35BE67A12FB9AD027F7CCDC2E5948BCD2D124D5EE16D4E4738F5B21589B576EE75418123BF09311BE563DDC3C1236CFAAB86977779780656FC41DE0AE7A6A2FD73CF2EA4FADB6669C88214C93E7B8D4E8DFA7460D121D5A4394FCA90EA947C963EEA88892282472C6B00B4B96B7DC23CA226FAD31567A5038408A40D94A84A38A62920A36341A956A4FDBFF79A9F6B65298493A8EBAEAD2346CDEC3D60BD2C64BB";
	
	//returns number of padding blocks
	numberOfPaddingBlocks = findPadding(ciphertext, iv, key);

	//converts IV to ascii
	iv = hexToAscii(iv);

	//transforms ciphertext into a vector of blocks
	std::vector<string> blocks = blockify(ciphertext, iv);
	
	system("pause"); cout << endl << endl << endl;

	int totalNumberOfBlocks = blocks.size();
	string currentBlock; string ivBlock;
	cout << totalNumberOfBlocks << endl;

	cout << "Now decrypting..." << endl;

	//attacks each block with it's IV
	for (int i = 0; i < totalNumberOfBlocks - 1; i++) {
		currentBlock = blocks[totalNumberOfBlocks - i - 1];
		ivBlock = blocks[totalNumberOfBlocks - i - 2];
		attackMessage(numberOfPaddingBlocks, currentBlock, ivBlock);
		numberOfPaddingBlocks = 0;
	}

	cout << "Decryption complete! Please check output.txt\n";

	//output to file
	int i = plaintext.size() - 1;
	for (; i >= 0; i--)
		fout << plaintext[i];

	system("pause"); 
	return 0;
}

//attacking the oracle
void attackMessage(int numberOfPaddingBlocks, std::string ciphertext, std::string iv) {
	char plaintextBlock[17] = { 0 };
	char currentByte;
	char newByte;
	int numberOfBytesInCurrentBlock = 16 - numberOfPaddingBlocks;

	//for each byte in the block that is not padding
	for (int j = 0; j < numberOfBytesInCurrentBlock; j++){

		//transform IV
		for (int i = 0; i < numberOfPaddingBlocks; i++) {
			currentByte = iv[15 - i];
			newByte = currentByte ^ numberOfPaddingBlocks ^ (numberOfPaddingBlocks + 1);
			iv[15 - i] = newByte;
	
		}
	
		currentByte = iv[15 - numberOfPaddingBlocks];
	
		//get correct iv
		for (int i = 0; i < 255; i++)
		{
			iv[15 - numberOfPaddingBlocks] = i;
			if (oracle(ciphertext, iv)) {
				break;
			}
		}

		
		//get plaintext byte
		plaintextBlock[15 - numberOfPaddingBlocks] = (numberOfPaddingBlocks + 1) ^ currentByte ^ iv[15 - numberOfPaddingBlocks];

		//increment number of padding blocks
		numberOfPaddingBlocks++;
	}

	plaintextBlock[16] = '\0';
	plaintext.push_back(plaintextBlock);
}

//finds padding
int findPadding(std::string &ciphertext, std::string iv, std::string key) {
	//conversion of iv and ciphertext to ascii
	iv = hexToAscii(iv);
	ciphertext = hexToAscii(ciphertext);

	//transforms ciphertext into blocks
	std::vector<string> blocks = blockify(ciphertext);
	//gets the last block and the block before it
	string lastBlock = blocks[blocks.size() - 1];
	string ivBlock = blocks[blocks.size() - 2];
	int i = 0;

	//queries the oracle until it gets an error to find the number of padding blocks
	for (; i < 16; i++) {
		ivBlock[i] += 1;
		if (!oracle(lastBlock, ivBlock)) {
			break;
		}
	}

	cout << "Number of padding blocks " << 16 - i << endl;

	return 16 - i;
}
