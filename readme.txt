Instructions to run:

1. The program is provided as a Visual Studio solution, as it uses 
specific libraries (Crypto++) which failed to run on other compilers. 
However, the raw source code can be found in a folder called "RawSource".

3. Currently the ciphertext is hardcoded within the program. This can
easily be replaced with another ciphertext, as long as it is encrypted
in AES in CBC mode.

